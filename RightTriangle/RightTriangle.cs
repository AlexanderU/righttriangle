﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RightTriangle
{
    public class RightTriangle
    {
        public double ComputeArea(double a, double b, double c)
        {
            double[] arr = { a, b, c };

            if (arr.Contains(0))
                throw new ArgumentException("Side of the triangle cannot be 0");

            Array.Sort(arr);
            if (Math.Pow(arr[2], 2) != (Math.Pow(arr[1], 2) + Math.Pow(arr[0], 2)))
                throw new ArgumentException("This is not a right triangle");

            return arr[0] * arr[1] / 2;
        }
    }
}
