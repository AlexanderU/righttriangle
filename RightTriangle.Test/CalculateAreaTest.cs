﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RightTriangle;

namespace RightTriangle.Test
{
    [TestClass]
    public class CalculateAreaTest
    {
        [TestMethod]
        public void Can_calculate()
        {
            double a = 3;
            double b = 4;
            double c = 5;
            RightTriangle target = new RightTriangle();

            double result = target.ComputeArea(a, c, b);

            Assert.AreEqual(6, result);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Can_calculate_with_incorrect_args()
        {
            RightTriangle target = new RightTriangle();

            target.ComputeArea(5, 2, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Can_calculate_with_zero_arg()
        {
            RightTriangle target = new RightTriangle();

            target.ComputeArea(0, 2, 4);
        }
    }
}
